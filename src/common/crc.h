#ifndef CRC_H
#define CRC_H

typedef uint8_t crc;

#define CRC_POLYNOMIAL 0xD8  /* 11011 followed by 0's */
#define CRC_WIDTH  (8 * sizeof(crc))
#define CRC_TOPBIT (1 << (CRC_WIDTH - 1))

crc crcSlow(uint8_t const message[], int nBytes);

#endif

