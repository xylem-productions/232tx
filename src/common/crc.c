#include <stdint.h>
#include "crc.h"

/*
 * The width of the CRC calculation and result.
 * Modify the typedef for a 16 or 32-bit CRC standard.
 */

crc crcSlow(uint8_t const message[], int nBytes)
{
    int b;
    uint8_t bit;

    crc  remainder = 0;
    for (b = 0; b < nBytes; ++b) {
        remainder ^= (message[b] << (CRC_WIDTH - 8));
        for (bit = 8; bit > 0; --bit) {
            if (remainder & CRC_TOPBIT) {
                remainder = (remainder << 1) ^ CRC_POLYNOMIAL;
            }
            else {
                remainder = (remainder << 1);
            }
        }
    }
    return (remainder);
}

