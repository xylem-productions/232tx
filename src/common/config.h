#ifndef COMMON_CONFIG_H
#define COMMON_CONFIG_H
#define VERSION "1.3"
#define DEBUG (0)
#define DEBUG_WAIT (0)
#define DEBUG_CONNECTION (0)

#define BUF_SZ (4096)

#define CHUNK_SIZE (256)
#define PROG_WIDTH (64)

/* Rate can be any of: 57600 38400 19200 9600 7200 3600 4800 2400 */
#define BAUD_RATE (57600)

#define RES "- (C)opyright 2024 XyleM Productions; All Rights Reserved."
#define BANNER "(v" VERSION ") " RES

#define NUM_HANDSHAKE_ATTEMPTS (20)

#endif /* defined COMMON_CONFIG_H */
