#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "config.h"

void uart_buf_read(void *buf, uint16_t len)
{
    crecv(buf, len);
}

uint8_t uart_buf_read_uint8_t()
{
    uint8_t c;
    crecv(&c, 1);
#if DEBUG
    printf("READ: %i. ", c);
    fflush(stdout);
#endif
    return c;
}

void uart_send_inline(uint8_t c)
{
    csend((void*)(&c), 1);
}

uint32_t uart_buf_read_uint32_t()
{
    uint32_t i;
    crecv((void *)&i, 4);
    return i;
}

uint16_t read_chunk(uint8_t *buf, uint16_t a)
{
    crc src_check;
    uint16_t num_failures;
    bool match = false;

    for(num_failures = 0; !match; num_failures++) {
        crecv(buf, a);
        crecv(&src_check, 1);
        match = (src_check == crcSlow(buf, a));
        if (!match) {
            /* report CRC mismatch */
#if DEBUG
            printf("\n\nSending mismatch signal\n");
            fflush(stdout);
#endif
            csend("C", 1);
        }
    }
    return num_failures;
}

int receive_data()
{
    FILE *fp;
    uint32_t sz;
    uint32_t a;
    uint32_t total;
    uint8_t buf[CHUNK_SIZE];
    char fname[13];
    uint8_t fname_sz;

#if DEBUG
    printf("Getting file name ...\r\n");
    fflush(stdout);
#endif
    fname_sz = uart_buf_read_uint8_t();
#if DEBUG
    printf("Filename size: %i\r\n", fname_sz);
    fflush(stdout);
#endif
    uart_buf_read(fname, fname_sz);

#if DEBUG
    printf("Opening output file ... ");
    fflush(stdout);
#endif
    fp = fopen(fname, "wb");
    if (fp == NULL) {
        printf("Could not open file: \"%s\"\r\n", fname);
        return 1;
    }

    /* send ACK */
    csend("!", 1);

#if DEBUG
    printf("OK.\r\nGetting file size ... ");
    fflush(stdout);
#endif

    sz = uart_buf_read_uint32_t();

    printf("File size: %u\n", sz);
    fflush(stdout);
    /* send ACK */
    csend("!", 1);

    /* receive file */
    printf("Receiving file \"%s\" (%u bytes):\r\n", fname, sz);
    fflush(stdout);
    for (total = 0; total < sz; total += a) {
        a = ((sz - total) < CHUNK_SIZE) ? (sz - total) : CHUNK_SIZE;

        /* read CHUNK_SIZE or "rest" */
#if DEBUG
        printf("Reading chunk of data ... ");
        fflush(stdout);
#endif
        read_chunk(buf, a);

        /* write to file */
#if DEBUG
        printf("OK.\r\nWriting to disk ... ");
        fflush(stdout);
#endif
        fwrite(buf, 1, a, fp);

        /* send a one byte ACK */
#if DEBUG
        printf("OK.\r\nSending ACK.\r\n");
        fflush(stdout);
#endif
        csend("!", 1);
        print_progress(total, sz);
    }
    print_progress(total, sz);
#if DEBUG
        printf("Closing file.\r\n");
        fflush(stdout);
#else
    printf("\r\n");
#endif
    fclose(fp);
    return 0;
}

void print_syntax()
{
    printf("\n");
    printf("Syntax: 232dn -P<name> [-B<rate>]\n\n");
    printf("  -P<name>  Source; TCP/IP port or TTY device filename\n");
    printf("  -B<rate>  Baud Rate. Useful values:\n");
    printf("                57600 38400 19200 9600 7200\n");
    printf("                4800 2400 1800 1200 600 300\n");
    printf("                Default: %u\n", BAUD_RATE);
    printf("\n");
}

int parse_args(int argc, char **argv,
        const char **con_name, uint32_t *baud_rate)
{
    int i;
    char *v;

    *baud_rate = BAUD_RATE;
    *con_name = NULL;
    for (i = 1; i < argc; i++) {
        v = argv[i];
        if(v[0] == '-') {
            switch(v[1]) {
                case 'b':
                case 'B':
                    *baud_rate = atol(&v[2]);
                    break;
                case 'p':
                case 'P':
                    *con_name = &v[2];
                    break;
                default:
                    print_syntax();
                    return 1;
            }
        }
    }
    if (*con_name == NULL) {
        print_syntax();
        return 1;
    }
    return 0;
}

int main(int argc, char **argv)
{
    uint32_t baud_rate;

    printf("232DN %s\n", BANNER);

    if (parse_args(argc, argv, &con_name, &baud_rate)) {
        return 0;
    }

    if(setup_connection_type(con_name)) {
        return 1;
    }
    if(open_connection(baud_rate)) {
        return 1;
    }
    if(receive_data()) {
        return 1;
    }
    printf("Transfer complete.\n");
    close_connection();
}

