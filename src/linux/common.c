#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include "rs232.h"
#include "crc.h"
#include "common.h"
#include "config.h"

extern char *comports[37];

char *con_type_name[] = { "NET", "UART", "UNIX" };

const char *con_name;
int con_port;
connection_type con_type;

uint8_t buf[BUF_SZ];
uint16_t buf_sz;

int portfd_tcp;
int portfd;
struct sockaddr_in tcp_server_sockaddr;
struct sockaddr_in tcp_client_sockaddr;
struct sockaddr_un unix_sockaddr;

/* function pointers for networking routines */
typedef int (*csend_fptr)(void*, uint16_t);
typedef int (*crecv_fptr)(unsigned char *, uint16_t);

csend_fptr csend;
crecv_fptr crecv;
crecv_fptr crecv_nonblock;

char get_char(int v)
{
    return (v >= 32 && v <= 126) ? v : '.';
}

void print_line(char *buf, int n)
{
    int i;
    for (i = 0; i < n; i++) {
        printf("%02x ", (unsigned char)buf[i]);
    }
    printf("| ");
    for (i = 0; i < n; i++) {
        printf("%c", get_char(buf[i]));
    }
    printf("\n");
}

int csend_net(void *buf, uint16_t len) {
    return write(portfd, buf, len);
}

int csend_uart(void *buf, uint16_t len) {
    return RS232_SendBuf(con_port, buf, len);
}

int crecv_net(unsigned char *buf, uint16_t len)
{
    uint16_t amount;

    for(amount = 0; amount < len;) {
        uint16_t a;
        a = read(portfd, buf, len - amount);
        buf += a;
        amount += a;
    }
    return len;
}

int crecv_net_nonblock(unsigned char *buf, uint16_t len)
{
    int ret;
    const int flags = fcntl(portfd, F_GETFL, 0);

    fcntl(portfd, F_SETFL, flags | O_NONBLOCK);
    ret = crecv_net(buf, len);
    fcntl(portfd, F_SETFL, flags ^ O_NONBLOCK);
    return ret;
}

int crecv_uart(unsigned char *buf, uint16_t len)
{
    unsigned char *b;
    uint16_t remainder;
    uint16_t c;

    for (remainder = len, b = buf; remainder;) {
        c = RS232_PollComport(con_port, b, remainder);
        if (c > 0) {
            b += c;
            if (c > remainder) {
                c = remainder;
            }
            remainder -= c;
        } else {
            usleep(15000);
        }
    }
    return len;
}

int crecv_uart_nonblock(unsigned char *buf, uint16_t len)
{
    unsigned char *b;
    uint16_t remainder;
    uint16_t c;
    uint16_t tries;

    tries = 3;
    for (remainder = len, b = buf; remainder;) {
        c = RS232_PollComport(con_port, b, remainder);
        if (c > 0) {
            b += c;
            if (c > remainder) {
                c = remainder;
            }
            remainder -= c;
        } else {
            tries--;
            if (!tries) {
                return (len - remainder);
            }
            usleep(15000);
        }
    }
    return len;
}

int handshake()
{
    uint8_t c;
    uint16_t a = 0;
    char spinner[5] = "/-\\|";

    usleep(500000);
    while (1) {
        csend("!", 1);
        crecv_nonblock(&c, 1);
#if DEBUG
        printf("%c", c);
        fflush(stdout);
#endif
        if (c == '!') {
            csend("$", 1);
            while (c == '!') {
                crecv(&c, 1);
#if DEBUG
                printf("%c", c);
                fflush(stdout);
#endif
            }
            if (c == '$') {
#if DEBUG
                printf("\n");
                fflush(stdout);
#endif
                return 0;
            }
            return 1;
        }
        printf("\rEstablishing handshake ... %c ", spinner[a++%4]);
        fflush(stdout);
        usleep(100000);
    }
    return 1;
}

void socket_close(void)
{
  close(portfd);
  portfd = -1;
}

int unix_socket_connect(void)
{
    if ((portfd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
      return 1;
    }

    if (connect(portfd, (struct sockaddr *)&unix_sockaddr,
              sizeof(unix_sockaddr)) == -1) {
        socket_close();
    }
    return 0;
}

int init_unix_socket()
{
    unix_sockaddr.sun_family = AF_UNIX;
    strcpy(unix_sockaddr.sun_path, con_name);
    unix_sockaddr.sun_path[sizeof(unix_sockaddr.sun_path)-1] = 0;

    csend = csend_net;
    crecv = crecv_net;
    crecv = crecv_net_nonblock;

    return unix_socket_connect();
}

int init_net()
{
    char spinloop[4] = "|/-\\";
    char spin_pos = 0;

    /* set function pointers */

    csend = csend_net;
    crecv = crecv_net;
    crecv_nonblock = crecv_net_nonblock;

    /* Create socket */

    portfd_tcp = socket(AF_INET, SOCK_STREAM, 0);
    if (portfd_tcp == -1) {
        printf("ERROR: Could not create socket.");
        return 1;
    }

    /* Prepare the sockaddr_in structure */

    tcp_server_sockaddr.sin_family = AF_INET;
    tcp_server_sockaddr.sin_addr.s_addr = INADDR_ANY;
    tcp_server_sockaddr.sin_port = htons(con_port);

    /* Bind */

    printf("Binding to client address ...  ");
    while(bind(
                portfd_tcp,
                (struct sockaddr *)&tcp_server_sockaddr,
                sizeof(tcp_server_sockaddr)) < 0) {
            printf("\rBinding to client address ... %c ",
                    spinloop[(spin_pos++)%4]);
            fflush(stdout);
            sleep(1);
    }

    /* Listen */

    listen(portfd_tcp, 3);
    printf("done.\n");
    return 0;
}

int init_uart(uint32_t bdrate)
{
    char mode[] = {'8', 'N', '1', 0};

    /* set function pointers */

    csend = csend_uart;
    crecv = crecv_uart;
    crecv_nonblock = crecv_uart_nonblock;

    if (RS232_OpenComport(con_port, (int)bdrate, mode, 0)) {
        return 1;
    }
    return 0;
}

int init_connection(connection_type con_type, uint32_t baud_rate)
{
    switch (con_type) {
        case CON_TYPE_NET:
            return init_net();
        case CON_TYPE_UNIX_SOCKET:
            return init_unix_socket();
        default:
            break;
    };
    return init_uart(baud_rate);
}

int prepare_connection()
{
    int i;

    /* Accept an incoming connection */

    if (con_type == CON_TYPE_NET) {
        i = sizeof(struct sockaddr_in);
        portfd = accept(
                portfd_tcp,
                (struct sockaddr *)&tcp_client_sockaddr,
                (socklen_t*)&i);
        if (portfd < 0) {
            perror("Accept failed.");
            return 1;
        }
    }
    printf("done.\n");
    /* clean up weird startup noise */
    printf("Establishing handshake ... ");
    fflush(stdout);
    if(handshake()) {
        printf("Aborted.\n");
        return 1;
    }
    printf("\rEstablishing handshake ... done.\n");
    fflush(stdout);
    return 0;
}

void zap_buf(int *p)
{
    int i;

    buf_sz -= *p;
    for (i = 0; i < buf_sz; i++) {
        buf[i] = buf[*p + i];
    }
    *p = 0;
}

void connection_read_all()
{
    uint8_t *b;
    int i;

#define r (sizeof(buf) - buf_sz)
    b = buf + buf_sz;
    for (i = crecv(b, r); i > 0; i = crecv(b, r)) {
        usleep(15000); /* provide enough sleep to ensure client can xmit */
        buf_sz += i;
        b += i;
    }
#undef r
}

bool valid_ack(uint8_t *valid, uint8_t v)
{
    uint8_t *p;

    for (p = valid; *p != '\0'; p++) {
        if (v == *p)
            return true;
    }
    return false;
}

uint8_t get_ack(uint8_t *valid)
{
    uint8_t ack;

#if DEBUG
    printf("Waiting for ACK ... ");
    fflush(stdout);
#endif
    for (ack = 0; !valid_ack(valid, ack);) {
        crecv(&ack, 1);
#if DEBUG
        printf("Received: \"%c\"\n", ack);
        fflush(stdout);
#endif
    }
    return ack;
}

void print_progress(uint32_t total, uint32_t sz)
{
    uint32_t prog = (uint32_t)(((float)total / (float)sz) * (float)PROG_WIDTH);

#if DEBUG
    printf("---- Iteration: total=%i; prog=%i\n", total, prog);
#else
    uint32_t i;

    printf(" [");
    for (i = 0; i < prog ; i++) printf("#");
    for (i = 0; i < PROG_WIDTH - prog; i++) printf(".");
    printf("] %3i%%\r", (int)(((float)total / (float)sz) * (float)100));
#endif
    fflush(stdout);
}

int determine_uart_port(const char *tty_name)
{
    size_t i;
    for (i=0; i < (sizeof(comports) / sizeof(char *)); i++) {
        if (!strcmp(comports[i], tty_name)) {
            return i;
        }
    }
    return -1;
}

int setup_connection_type(const char *con_name)
{
    con_port = atoi(con_name);
    con_type = CON_TYPE_NET;
    if (con_port < 1) {
        /* Not a number so not a TCP/IP port; treat as a TTY filename */
        con_port = determine_uart_port(con_name);
        con_type = CON_TYPE_UART;
        if (con_port < 0) {
            /* Not a valid UART connection name, treat as a UNIX socket */
            con_port = 1337;
            con_type = CON_TYPE_UNIX_SOCKET;
        }
    }
    return 0;
}

int open_connection(uint32_t baud_rate) {
    printf("Connecting to port %i; Connection type: %s (%i 8N1) ...\n",
            con_port, con_type_name[con_type], baud_rate);

    if(init_connection(con_type, baud_rate)) {
        printf("Error initializing connection.\n");
        return 1;
    }
    /* Prepare a connection with the client */
    printf("Waiting for connection ... ");
    fflush(stdout);
    if (prepare_connection()) {
        printf("ERROR: Failed connection.\n");
        return 1;
    }
    return 0;
}

void close_connection() {
    switch (con_type) {
        case CON_TYPE_NET:
            socket_close();
            close(portfd_tcp);
            break;
        case CON_TYPE_UNIX_SOCKET:
            socket_close();
            break;
        default:
            break;
    };
}

