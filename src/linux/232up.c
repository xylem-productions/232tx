#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "common.h"
#include "config.h"

char* bname(char *n)
{
    /* roll your own basename that doesn't crash. (TM) */

    int i;
    char *p;

    i = strlen(n);
    for (p = &n[i]; p != n; p--) {
        if (*p == '/') {
            return p + 1;
        }
    }
    return n;
}

uint16_t send_chunk(uint8_t *buf, uint16_t a, crc check)
{
    uint16_t num_failures;
    uint8_t ack;

    for (num_failures = 0, ack = 0; ack != '!'; num_failures++) {
        csend(buf, a);
        csend(&check, 1);
#if DEBUG
    {
        int i;

        printf("sent %i bytes: ", a);
        for (i = 0; i < a; i++) {
            printf(" %i", (int)buf[i]);
        }
        printf("\n");
        fflush(stdout);
    }
#endif
        ack = get_ack((uint8_t *)"!C");
    }
    return num_failures;
}

int file_exists(const char *fname)
{
    struct stat buffer;
    return (stat(fname, &buffer) == 0) ? 1 : 0;
}

int transfer_data(const char *fname)
{
    FILE *fp;
    uint32_t sz;
    size_t a;
    uint32_t total;
    uint8_t buf[CHUNK_SIZE];
    char *bn;
    crc check;

    fp = fopen(fname, "rb");
    if (fp == NULL) {
        printf("File not found.\n");
        return 1;
    }

    /* send filename length (including terminator) and filename */
    bn = bname((char *)fname);
    sz = strlen(bn) + 1;
    csend(&sz, 1);
    csend((void *)bn, sz);
    get_ack((uint8_t *)"!");

    /* send file length (32 bit integer) 4 bytes */
    fseek(fp, 0L, SEEK_END);
    sz = ftell(fp);
    fseek(fp, 0L, SEEK_SET);
    csend(&sz, 4);
    get_ack((uint8_t *)"!");

    /* send all the bytes */

    printf("Transferring file \"%s\" (%i bytes):\n", bn, sz);
    fflush(stdout);
    for (total = 0; total < sz; total += a) {
        a = fread(buf, 1, CHUNK_SIZE, fp);
        check = crcSlow(buf, a);
        send_chunk(buf, a, check);
        print_progress(total, sz);
    }
    print_progress(total, sz);
#if DEBUG
    printf("Transfer complete, closing file.\n");
#else
    printf("\r\n");
#endif
    fflush(stdout);
    fclose(fp);
    return 0;
}

void print_syntax()
{
    printf("\n");
    printf("Syntax: 232up -P<name> [-B<rate>] FNAME\n\n");
    printf("  -P<name>  Destination; TCP/IP port or TTY device filename\n");
    printf("  -B<rate>  Baud Rate. Useful values:\n");
    printf("                57600 38400 19200 9600 7200\n");
    printf("                4800 2400 1800 1200 600 300\n");
    printf("                Default: %u\n", BAUD_RATE);
    printf("  FNAME     File to transfer\n");
    printf("\n");
}

int parse_args(int argc, char **argv,
        const char **con_name, const char **fname, uint32_t *baud_rate)
{
    int i;
    char *v;

    *baud_rate = BAUD_RATE;
    *fname = NULL;
    *con_name = NULL;
    for (i = 1; i < argc; i++) {
        v = argv[i];
        if(v[0] == '-') {
            switch(v[1]) {
                case 'b':
                case 'B':
                    *baud_rate = atol(&v[2]);
                    break;
                case 'p':
                case 'P':
                    *con_name = &v[2];
                    break;
                default:
                    print_syntax();
                    return 1;
            }
        }
        else {
            /* Assume this is a file name */
            *fname = v;
        }
    }
    if (*fname == NULL || *con_name == NULL) {
        print_syntax();
        return 1;
    }
    return 0;
}

int main(int argc, char **argv)
{
    const char *fname;
    uint32_t baud_rate;

    printf("232UP %s\n", BANNER);

    if (parse_args(argc, argv, &con_name, &fname, &baud_rate)) {
        return 0;
    }
    if (!file_exists(fname)) {
        printf("Invalid filename.\n");
        return 1;
    }
    if(setup_connection_type(con_name)) {
        return 1;
    }
    if(open_connection(baud_rate)) {
        return 1;
    }
    if(transfer_data(fname)) {
        return 1;
    }
    printf("Transfer complete.\n");
    close_connection();
}

