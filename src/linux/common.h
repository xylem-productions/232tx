#ifndef COMMON_H
#define COMMON_H

#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include "crc.h"

typedef enum {
    CON_TYPE_NET = 0,
    CON_TYPE_UART,
    CON_TYPE_UNIX_SOCKET
} connection_type;

/* function pointers for networking routines */
typedef int (*csend_fptr)(void*, uint16_t);
typedef int (*crecv_fptr)(unsigned char *, uint16_t);

int init_connection(connection_type con_type, uint32_t baud_rate);
uint8_t get_ack(uint8_t *valid);
void print_progress(uint32_t total, uint32_t sz);
//int determine_uart_port(const char *tty_name);
int setup_connection_type(const char *con_name);
int open_connection(uint32_t baud_rate);
void close_connection();

extern const char *con_name;
extern int con_port;
extern connection_type con_type;
extern csend_fptr csend;
extern crecv_fptr crecv;

#endif

