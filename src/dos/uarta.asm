CPU 8086

global uart_init_
global uart_reset_
global uart_send_

extern _uart_ringbuf
extern _uart_idx
extern _uart_buf_count
extern _uart_rate_divider
extern _old_int12_seg
extern _old_int12_off

; COM1 constants

COM1_PORT    equ 0x3f8          ; base address of COM1 port
COM1_IRQ     equ 4              ; COM1 IRQ
COM1_INT     equ 0x0C           ; IRQ 4, COM1, interrupt 12 (0x0C)
COM1_INT_LOC equ COM1_INT * 4   ; Location in memory of interrupt 12 vector

; COM2 constants

COM2_PORT    equ 0x2f8          ; base address of COM2 port
COM2_IRQ     equ 3              ; COM2 IRQ
COM2_INT     equ 0x0B           ; IRQ 3, COM2, interrupt 11 (0x0B)
COM2_INT_LOC equ COM2_INT * 4   ; Location in memory of interrupt 11 vector

; UART base address offsets

THR equ 0 ; transmitter holding buffer (w)
RBR equ 0 ; receive buffer (r)
IER equ 1 ; interrupt enable register (r/w)
FCR equ 2 ; FIFO control register (w)
LCR equ 3 ; line control register (r/w)
MCR equ 4 ; modem control register (r/w)
LSR equ 5 ; line status register (r)
MSR equ 6 ; modem status register (r)

; Timer goes at 115200 tps, these are divisors for baud rate

RATE_115200 equ 0x01
RATE_57600  equ 0x02
RATE_19200  equ 0x06
RATE_14400  equ 0x08
RATE_9600   equ 0x0C
RATE_7200   equ 0x10
RATE_3600   equ 0x20
RATE_4800   equ 0x24
RATE_2400   equ 0x30

; Bit settings

BITS_5N1 equ 0
BITS_6N1 equ 1
BITS_7N1 equ 2
BITS_8N1 equ 3

LATCH_LOW  equ 0
LATCH_HIGH equ 1

; PIC constants

MASTERPIC  equ 0x20
MASTEROCW1 equ 0x21

SEGMENT .code0 USE16 CLASS=CODE

saved_ds: dw 0

; send character, al: value
uart_send_:
  push dx
  push ax

  ; wait for the buffer to be empty
  mov dx, COM1_PORT + LSR
filled:
  in al, dx
  and al, 0x20 ; test 6th bit, if it's set, transmit buffer is empty
  jz filled

  ; write character
  cli
  ;  Write_UART(port_in_use, REG_THR, (int)ch);
  mov dx, COM1_PORT + THR
  pop ax
  out dx, al
  sti

  pop dx
  ret

uart_reset_:
  push ax
  push dx

  ; disable "Received Data Available" Interruption

  mov dx, COM1_PORT + IER
  mov al, 0
  out dx, al

  ; disable interrupt for IRQ4

  mov dx, MASTEROCW1
  in al, dx
  or al, 0x10 ; 00010000
  out dx, al

  ; reinstate original interrupt for IRQ4
  cli
  mov dx, [_old_int12_off]
  mov ah, 0x25
  mov al, COM1_INT
  int 0x21
  sti

  pop dx
  pop ax
  mov ds, [cs:saved_ds]
  ret

uart_init_:

  cli
  push ax
  push dx

  ; get existing IRQ4 interrupt handler into ES:BX

  push es
  push bx
  mov ah, 0x35
  mov al, COM1_INT
  int 0x21

  ; save existing IRQ4 interrupt to old_int12_seg:old_int12_off

  mov [_old_int12_seg], es
  mov [_old_int12_off], bx
  pop bx
  pop es

  ; install new IRQ4 interrupt handler from DS:DX

  push ds
  mov [cs:saved_ds], ds     ; save real ds
  push cs
  pop ds                    ; set ds=cs
  mov dx, new_int12
  mov ah, 0x25
  mov al, COM1_INT
  int 0x21
  pop ds

  ; change UART settings

  ; set baud rate
  mov al, 0x80
  mov dx, COM1_PORT + LCR
  out dx, al
  mov al, 0
  mov dx, COM1_PORT + LATCH_HIGH
  out dx, al
  mov al, [_uart_rate_divider]
  mov dx, COM1_PORT + LATCH_LOW
  out dx, al

  ; set bits (with 1 stop bit, no parity)
  mov al, BITS_8N1
  mov dx, COM1_PORT + LCR
  out dx, al

  ; Enable FIFO, clear them, with 14-byte threshold
  mov al, 0xC7
  mov dx, COM1_PORT + FCR
  out dx, al
  mov al, 0
  out dx, al

  ; Data Terminal Ready, Request To Send, Enable IRQ
  mov al, 0x0B
  mov dx, COM1_PORT + MCR
  out dx, al

  ; enable interrupt for IRQ4

  mov dx, MASTEROCW1
  in al, dx
  and al, 0xef ; 11101111
  out dx, al

  ; Enable "Received Data Available" Interruption

  mov dx, COM1_PORT + IER
  mov al, 1
  out dx, al

  pop dx
  pop ax
  sti
  ret

uart_poll:
  push ax
  push cx
  push dx
  push si
  push ds
  mov ds, [cs:saved_ds]

readmore:

  ; check if there is something to read

  mov dx, COM1_PORT + LSR
  in al, dx
  and al, 0x01
  jz nothing

  ; read stuff

  mov dx, COM1_PORT + RBR
  in al, dx

  mov cx, [_uart_idx]
  push ax
  mov ax, cx
  lea si, [_uart_ringbuf]
  add si, ax
  pop ax
  mov [si], al
  inc cx
  and cx, 0xfff
  mov [_uart_idx], cx
  inc word [_uart_buf_count]

  jmp readmore

nothing:
  pop ds
  pop si
  pop dx
  pop cx
  pop ax
  ret

new_int12:
  cli
  push ax
  push dx
  call uart_poll
  mov al, 0x20 ; EOI
  out MASTERPIC, al
  pop dx
  pop ax
  sti
  iret

