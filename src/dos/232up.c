#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "uartc.h"
#include "common.h"
#include "config.h"

int file_exists(const char *fname)
{
    FILE *fp;

    fp = fopen(fname, "rb");
    if (fp == NULL) {
        return 0;
    }
    fclose(fp);
    return 1;
}

int transfer_data(const char *fname)
{
    FILE *fp;
    uint32_t sz;
    uint32_t a;
    uint32_t total;
    uint8_t buf[CHUNK_SIZE];
    const char *bn;
    crc check;

    fp = fopen(fname, "rb");
    if (fp == NULL) {
        printf("File not found.\n");
        return 1;
    }

    /* send filename length (including terminator) and filename */
    bn = fname;
    sz = strlen(bn) + 1;
    csend(&sz, 1);
    csend((void *)bn, sz);
    get_ack("!");

    /* send file length (32 bit integer) 4 bytes */
    fseek(fp, 0L, SEEK_END);
    sz = ftell(fp);
    fseek(fp, 0L, SEEK_SET);
    csend(&sz, 4);
    get_ack("!");

    /* send all the bytes */

    printf("Transferring file \"%s\" (%lu bytes):\n", bn, sz);
    fflush(stdout);
    for (total = 0; total < sz; total += a) {
        a = fread(buf, 1, CHUNK_SIZE, fp);
        check = crcSlow(buf, a);
        send_chunk(buf, a, check);
        print_progress(total, sz);
    }
    print_progress(total, sz);
#if DEBUG
    printf("Transfer complete, closing file.\n");
#else
    printf("\r\n");
#endif
    fflush(stdout);
    fclose(fp);
    return 0;
}

void print_syntax()
{
    printf("\r\n");
    printf("Syntax: 232up [/B<rate>] FNAME\r\n\r\n");
    printf("  /B<rate>  Baud Rate. Useful values:\r\n");
    printf("                57600 38400 19200 9600 7200\n");
    printf("                4800 2400 1800 1200 600 300\n");
    printf("                Default: %lu\r\n", (uint32_t)(BAUD_RATE));
    printf("  FNAME     File to transfer; must be in the directory\r\n");
}

int parse_args(int argc, char **argv, char **fname, uint32_t *baud_rate)
{
    int i;
    char *v;

    *baud_rate = BAUD_RATE;
    *fname = NULL;
    for (i = 1; i < argc; i++) {
        v = argv[i];
        if(v[0] == '/') {
            switch(v[1]) {
                case 'b':
                case 'B':
                    *baud_rate = atol(&v[2]);
                    break;
                default:
                    print_syntax();
                    return 1;
            }
        }
        else {
            /* assume it is the file name */
            printf("File name: %s\r\n", v);
            *fname = v;
        }
    }
    if (*fname == NULL) {
        print_syntax();
        return 1;
    }
    return 0;
}

int main(int argc, char **argv) {
    char *fname;
    uint32_t baud_rate;

    printf("232UP %s\r\n", BANNER);
    if(parse_args(argc, argv, &fname, &baud_rate)) {
        return 0;
    }
    if (!file_exists(fname)) {
        printf("Invalid filename.\r\n");
        return 1;
    }
    printf("Preparing to send \"%s\"\r\n", fname);
    if(prepare_connection(baud_rate)) {
        return 1;
    }
    if(transfer_data(fname)) {
        return 1;
    }
    printf("Transfer complete.\r\n");
    reset_uart();
    return 0;
}

