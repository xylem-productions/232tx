#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include "uartc.h"
#include "crc.h"
#include "common.h"
#include "config.h"

void print_progress(uint32_t total, uint32_t sz)
{
    int prog = (int)(((float)total / (float)sz) * (float)PROG_WIDTH);

#if DEBUG
    printf("---- Iteration: total=%lu; prog=%lu\r\n", total, prog);
#else
    uint32_t i;

    printf(" [");
    for (i = 0; i < prog; i++) printf("#");
    for (i = 0; i < PROG_WIDTH - prog; i++) printf(".");
    printf("] %3i%%\r", (int)(((float)total / (float)sz) * (float)100));
#endif
    fflush(stdout);
}

void csend(void *buf, uint16_t len) {
    uint8_t *b;

    b = (uint8_t *)buf;

    for(; len > 0; len--) {
#if DEBUG
        printf("SENDING %i. ", *b);
        fflush(stdout);
#endif
        uart_send_inline(*b);
        b++;
    }
}

void crecv(uint8_t *buf, uint16_t len) {
    for (; len > 0; len--) {
        *buf = uart_buf_read_uint8_t();
        buf++;
    }
}

bool valid_ack(uint8_t *valid, uint8_t v)
{
    uint8_t *p;

    for (p = valid; *p != '\0'; p++) {
        if (v == *p)
            return true;
    }
    return false;
}

uint8_t get_ack(uint8_t *valid)
{
    uint8_t ack;

#if DEBUG
    printf("Waiting for ACK ... ");
    fflush(stdout);
#endif
    for (ack = 0; !valid_ack(valid, ack);) {
        crecv(&ack, 1);
#if DEBUG
        printf("Received: \"%c\"\n", ack);
        fflush(stdout);
#endif
    }
    return ack;
}

uint16_t read_chunk(uint8_t *buf, uint16_t a)
{
    crc src_check;
    uint16_t num_failures;
    bool match = false;

    for(num_failures = 0; !match; num_failures++) {
        uart_buf_read(buf, a);
        src_check = uart_buf_read_uint8_t();
        match = (src_check == crcSlow(buf, a));
        if (!match) {
            /* report CRC mismatch */
            uart_send_inline('C');
        }
    }
    return num_failures;
}

uint16_t send_chunk(uint8_t *buf, uint16_t a, crc check)
{
    uint16_t num_failures;
    uint8_t ack;

    for (num_failures = 0, ack = 0; ack != '!'; num_failures++) {
        csend(buf, a);
        csend(&check, 1);
#if DEBUG
    {
        int i;

        printf("sent %i bytes: ", a);
        for (i = 0; i < a; i++) {
            printf(" %i", (int)buf[i]);
        }
        printf("\n");
        fflush(stdout);
    }
#endif
        ack = get_ack("!C");
#if DEBUG
        if (ack == 'C') {
            printf("\r\n\r\nCRC mismatch reported \r\n");
            fflush(stdout);
        }
#endif
    }
    return num_failures;
}

int prepare_connection(uint32_t baud_rate)
{
    printf("Initializing UART (%lu 8N1) ... ", baud_rate);
    fflush(stdout);
    init_uart(baud_rate);
    printf("done.\r\n");
    fflush(stdout);
    printf("Establishing handshake ... ");
    fflush(stdout);
    if(handshake()) {
        printf("Aborted.\r\n");
        return 1;
    }
    printf("\rEstablishing handshake ... done.\r\n");
    fflush(stdout);
    return 0;
}

