#ifndef UART_C_H
#define UART_C_H

#define UART_RINGBUF_MAX (0xfff)

extern void uart_init(void);
extern void uart_reset(void);
extern void uart_send(void);

#define UART_P(p) (&uart_ringbuf[uart_current_idx + p])

static void uart_send_inline(char ch);
#pragma aux uart_send_inline = \
    "call uart_send" \
    parm [al]

static char uart_check_inline();
#pragma aux uart_check_inline = \
"   mov dx, 3fdh" \
"   in al, dx" \
"   and al, 1" \
    modify [ dx ] \
    value [ al ]

static char uart_get_inline();
#pragma aux uart_get_inline = \
"   mov dx, 3f8h" \
"   in al, dx" \
    modify [ dx ] \
    value [ al ]

void init_uart(uint16_t rate);
void reset_uart();

uint16_t get_uart_buf_count();
int advance_uart_current_idx(int n);
void wait_for_uart(int sz);

void uart_send_string(char *s);
void uart_send_uint16(uint16_t r);

void uart_buf_read(uint8_t *buf, uint16_t amount);
uint8_t uart_buf_read_uint8_t();
uint16_t uart_buf_read_uint16_t();
uint32_t uart_buf_read_uint32_t();
int handshake();

#endif
