#ifndef BYTES_H
#define BYTES_H

typedef union b_uint16_t b_uint16_t;
union b_uint16_t {
    uint16_t v;
    char b[2];
};

typedef union b_uint32_t b_uint32_t;
union b_uint32_t {
    uint32_t v;
    char b[4];
};

#endif
