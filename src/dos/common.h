#ifndef COMMON_H
#define COMMON_H

#include <unistd.h>
#include "crc.h"

void print_progress(uint32_t total, uint32_t sz);
uint16_t read_chunk(uint8_t *buf, uint16_t a);
uint16_t send_chunk(uint8_t *buf, uint16_t a, crc check);
int prepare_connection(uint32_t baud_rate);
void csend(void *buf, uint16_t len);
uint8_t get_ack(uint8_t *valid);

#endif

