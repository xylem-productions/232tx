#include <unistd.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "uartc.h"
#include "common.h"
#include "config.h"

int receive_data()
{
    FILE *fp;
    uint32_t sz;
    uint32_t a;
    uint32_t total;
    uint8_t buf[CHUNK_SIZE];
    char fname[13];
    uint8_t fname_sz;

#if DEBUG
    printf("Getting file name ...\r\n");
    fflush(stdout);
#endif
    fname_sz = uart_buf_read_uint8_t();
    uart_buf_read(fname, fname_sz);

#if DEBUG
    printf("Opening output file ... ");
    fflush(stdout);
#endif
    fp = fopen(fname, "wb");
    if (fp == NULL) {
        printf("Could not open file: \"%s\"\r\n", fname);
        return 1;
    }

    /* send ACK */
    uart_send_inline('!');

#if DEBUG
    printf("OK.\r\nGetting file size ... ");
    fflush(stdout);
#endif

    sz = uart_buf_read_uint32_t();

    /* send ACK */
    uart_send_inline('!');

    /* receive file */
    printf("Receiving file \"%s\" (%lu bytes):\r\n", fname, sz);
    fflush(stdout);
    for (total = 0; total < sz; total += a) {
        a = ((sz - total) < CHUNK_SIZE) ? (sz - total) : CHUNK_SIZE;

        /* read CHUNK_SIZE or "rest" */
#if DEBUG
        printf("Reading chunk of data ... ");
        fflush(stdout);
#endif
        read_chunk(buf, a);

        /* write to file */
#if DEBUG
        printf("OK.\r\nWriting to disk ... ");
        fflush(stdout);
#endif
        fwrite(buf, 1, a, fp);

        /* send a one byte ACK */
#if DEBUG
        printf("OK.\r\nSending ACK.\r\n");
        fflush(stdout);
#endif
        uart_send_inline('!');
        print_progress(total, sz);
    }
    print_progress(total, sz);
#if DEBUG
        printf("Closing file.\r\n");
        fflush(stdout);
#else
    printf("\r\n");
#endif
    fclose(fp);
    return 0;
}

void print_syntax()
{
    printf("\r\n");
    printf("Syntax: 232dn [/B<rate>]\r\n\r\n");
    printf("  /B<rate>  Baud Rate. Useful values:\r\n");
    printf("                57600 38400 19200 9600 7200\r\n");
    printf("                4800 2400 1800 1200 600 300\r\n");
    printf("                Default: %lu\r\n", (uint32_t)(BAUD_RATE));
}

int parse_args(int argc, char **argv, uint32_t *baud_rate)
{
    int i;
    char *v;

    *baud_rate = BAUD_RATE;
    for (i = 1; i < argc; i++) {
        v = argv[i];
        if(v[0] == '/') {
            switch(v[1]) {
                case 'b':
                case 'B':
                    *baud_rate = atol(&v[2]);
                    break;
                default:
                    print_syntax();
                    return 1;
            }
        }
    }
    return 0;
}

int main(int argc, char **argv) {
    uint32_t baud_rate;

    printf("232DN %s\r\n", BANNER);
    if (parse_args(argc, argv, &baud_rate)) {
        return 0;
    }
    if(prepare_connection(baud_rate)) {
        return 1;
    }
    if(receive_data()) {
        return 1;
    }
    printf("Transfer complete.\r\n");
    reset_uart();
    return 0;
}

