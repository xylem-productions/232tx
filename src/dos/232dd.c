#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "uartc.h"
#include "common.h"
#include "config.h"

#define DISK_PROG_WIDTH (50)

void print_disk_progress(uint32_t sent, uint32_t sz, int c, int h, int s) {
    static unsigned char buf[128];
    int i;
    float frac = ((float)sent)/sz;
    int prog = (int)(frac*DISK_PROG_WIDTH);

    sprintf(buf, "(%3i,%3i,%3i)", c, h, s);
    for (i = strlen(buf); i < 13; i++) sprintf(buf, "%s ", buf);
    sprintf(buf, "%s [", buf);
    for (i = 0; i < prog; i++) sprintf(buf, "%s#", buf);
    for (i = 0; i < DISK_PROG_WIDTH - prog; i++) sprintf(buf, "%s.", buf);
    sprintf(buf, "%s] %3i%%", buf, (int)(frac*100));
    printf("%s\r", buf);
    fflush(stdout);
}

typedef struct DRV_params DRV_params;

struct DRV_params {
  unsigned char status;
  unsigned char type;
  unsigned int cylinders;
  unsigned char heads;
  unsigned char sectors;
  unsigned char num_drives;
};

unsigned char DRV_get_params(DRV_params *p, int idx);
#pragma aux DRV_get_params parm [si] [dl] value [ah] = \
  "mov ah, 0x8" \
  "or dl, 0x80" \
  "xor di, di" \
  "mov es, di" \
  "int 0x13" \
  "mov [si + 0], ah" \
  "jc @@exit" \
  "mov [si + 1], bl" \
  "mov al, ch" \
  "mov ah, cl" \
  "shr ah, 1" \
  "shr ah, 1" \
  "shr ah, 1" \
  "shr ah, 1" \
  "shr ah, 1" \
  "shr ah, 1" \
  "mov [si + 2], ax" \
  "mov [si + 4], dh" \
  "and cl, 0x3f" \
  "mov [si + 5], cl" \
  "mov [si + 6], dl" \
"@@exit:" \
  "push cs" \
  "pop es" \
  modify [ax bx cx dx di es]

int DRV_read_sector(unsigned char *buf, unsigned char idx, int c,
        unsigned char h, unsigned char s, unsigned char n);
#pragma aux DRV_read_sector parm [bx] [dl] [cx] [dh] [ah] [al] value [ax] = \
  "or dl, 0x80" \
  "xchg cl, ch" \
  "shl cl, 1" \
  "shl cl, 1" \
  "shl cl, 1" \
  "shl cl, 1" \
  "shl cl, 1" \
  "shl cl, 1" \
  "or cl, ah" \
  "mov ah, 0x2" \
  "int 0x13" \
  modify [ax cx dx bx]

#define SECTOR_SZ (512)

#define DRV_size(p) \
 (((uint32_t)SECTOR_SZ)*((p)->cylinders + 1)*((p)->heads + 1)*(p)->sectors)

int transfer_disk(DRV_params *p, int idx)
{
    uint32_t sz;
    int c, h, s;
    uint32_t sent;
    uint8_t buf[CHUNK_SIZE];
    const char *bn;

    /* send backup filename length (including terminator) and filename */
    sprintf(buf, "hdd%i.bin", idx);
    bn = buf;
    sz = strlen(bn) + 1;
    csend(&sz, 1);
    csend((void *)bn, sz);
    get_ack("!");

    /* send disk size as (32 bit integer) 4 bytes */
    sz = DRV_size(p);
    csend(&sz, 4);
    get_ack("!");

    /* send all the bytes */

    printf("Transferring %lu bytes as \"%s\":\n", sz, bn);
    fflush(stdout);
    for (sent = c = 0; c <= p->cylinders; c++) {
        for (h = 0; h <= p->heads; h++) {
            for (s = 1; s <= p->sectors; s++) {
                static unsigned char buf[512];
                int ret;
                crc check;
                /* Read 512 byte sector */
                ret = DRV_read_sector(buf, idx, c, h, s, 1);
                if (ret != 1) {
                    printf("Err = %4x, exiting");
                    return 1;
                }
                /* Send first 256 bytes */
                check = crcSlow(buf, 256);
                send_chunk(buf, 256, check);
                sent += 256;
                print_disk_progress(sent, sz, c, h, s);
                /* Send second 256 bytes */
                check = crcSlow(buf + 256, 256);
                send_chunk(buf + 256, 256, check);
                sent += 256;
                print_disk_progress(sent, sz, c, h, s);
            }
        }
    }
    printf("\r\n");
    fflush(stdout);
    return 0;
}

void print_syntax()
{
    printf("\r\n");
    printf("Syntax: 232dd [/B<rate>] HDD\r\n\r\n");
    printf("  /B<rate>  Baud Rate. Useful values:\r\n");
    printf("                57600 38400 19200 9600 7200\n");
    printf("                4800 2400 1800 1200 600 300\n");
    printf("                Default: %lu\r\n", (uint32_t)(BAUD_RATE));
    printf("  HDD       Hard drive index (default 0)\r\n");
}

int parse_args(int argc, char **argv, uint32_t *baud_rate,
        int *idx, DRV_params *p)
{
    int i;
    char *v;

    *baud_rate = BAUD_RATE;
    *idx = 0;
    for (i = 1; i < argc; i++) {
        v = argv[i];
        if(v[0] == '/') {
            switch(v[1]) {
                case 'b':
                case 'B':
                    *baud_rate = atol(&v[2]);
                    break;
                default:
                    print_syntax();
                    return 1;
            }
        }
        else {
            /* assume it is the hdd index */
            printf("HDD: %s\r\n", v);
            *idx = atoi(v);
        }
    }
    DRV_get_params(p, *idx);
    if (p-> status != 0) {
      printf("\r\nDrive not found, HDD = %i\r\n", *idx);
      print_syntax();
      return 1;
    }
    return 0;
}

int main(int argc, char **argv) {
    uint32_t baud_rate;
    int idx;
    DRV_params p;

    printf("232DD %s\r\n", BANNER);
    if(parse_args(argc, argv, &baud_rate, &idx, &p)) {
        return 0;
    }
    printf("Preparing to send HDD%i (%i,%i,%i) = %li bytes\r\n",
     idx, p.cylinders + 1, p.heads + 1, p.sectors, DRV_size(&p));
    if(prepare_connection(baud_rate)) {
        return 1;
    }
    if(transfer_disk(&p, idx)) {
        return 1;
    }
    printf("Transfer complete.\r\n");
    reset_uart();
    return 0;
}

