#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "bytes.h"
#include "kb.h"
#include "uartc.h"
#include "config.h"

#if DEBUG_WAIT
#include <stdio.h>
#endif

uint16_t uart_current_idx;
uint16_t uart_idx;
uint16_t uart_buf_count;
uint8_t uart_ringbuf[UART_RINGBUF_MAX + 1];
uint16_t old_int12_seg;
uint16_t old_int12_off;
uint8_t uart_rate_divider;

void init_uart(uint16_t rate)
{
    uart_idx = 0;
    uart_buf_count = 0;
    uart_current_idx = 0;
    uart_rate_divider = 115200 / rate;
    uart_init();
}

void reset_uart()
{
    uart_reset();
}

int advance_uart_current_idx(int n)
{
    if (uart_buf_count < n) {
        return 1;
    }
    uart_buf_count -= n;
    uart_current_idx += n;
    if (uart_current_idx > UART_RINGBUF_MAX) {
        uart_current_idx -= UART_RINGBUF_MAX + 1;
    }
    return 0;
}

void wait_for_uart(int sz)
{
    for(; get_uart_buf_count() < sz; ) {
#if DEBUG_WAIT
        int i;
        printf("w: %i; s: %i; b:", sz, uart_buf_count);
        for (i = 0; i < sz; i++) {
            printf(" %02x", uart_ringbuf[uart_current_idx + i]);
        }
        printf("\r\n");
        fflush(stdout);
#endif
    }
}

void uart_buf_read(uint8_t *buf, uint16_t amount)
{
    wait_for_uart(amount);
    if ((uart_current_idx + amount) <= UART_RINGBUF_MAX) {
        /* no rollover */
        memcpy(buf, &uart_ringbuf[uart_current_idx], amount);
    }
    else {
        uint16_t a = UART_RINGBUF_MAX - uart_current_idx + 1;

        memcpy(buf, &uart_ringbuf[uart_current_idx], a);
        if (amount > a) {
            memcpy(buf + a, uart_ringbuf, amount - a);
        }
    }
    advance_uart_current_idx(amount);
}

uint8_t uart_buf_read_uint8_t()
{
    uint8_t c;
    wait_for_uart(1);
    c = uart_ringbuf[uart_current_idx];
    advance_uart_current_idx(1);
    return c;
}

uint16_t uart_buf_read_uint16_t()
{
    b_uint16_t vi;
    uart_buf_read(&vi.b, 2);
    return vi.v;
}

uint32_t uart_buf_read_uint32_t()
{
    b_uint32_t vi;
    uart_buf_read(&vi.b, 4);
    return vi.v;
}

void uart_send_string(char *s)
{
    for (; *s != '\0'; s++) {
        uart_send_inline(*s);
    }
}

void uart_send_uint16(uint16_t r)
{
    uart_send_inline((char)(r>>8));
    uart_send_inline((char)r);
}

uint16_t get_uart_buf_count()
{
    return uart_buf_count;
}

int handshake()
{
    char c;
    int j;
    int a = 0;
    char spinner[5] = "/-\\|";

    delay(500);
    while (1) {
        uart_send_inline('!');
        if(uart_idx != uart_current_idx)
            c = uart_buf_read_uint8_t();
#if DEBUG
        printf("%c", c);
        fflush(stdout);
#endif
        if (c == '!') {
            uart_send_inline('$');
            for (j = 0; j < NUM_HANDSHAKE_ATTEMPTS; j++) {
                if (c != '!') {
                     break;
                }
                c = uart_buf_read_uint8_t();
#if DEBUG
                printf("%c", c);
                fflush(stdout);
#endif
            }
            if (c == '$') {
#if DEBUG
                printf("\r\n");
                fflush(stdout);
#endif
                return 0;
            }
            return 1;
        }
        printf("\rEstablishing handshake ... %c ", spinner[a++%4]);
        fflush(stdout);
        if (KB_keystroke()) {
            KB_getchar_ext();
            break;
        }
        delay(100);
    }
    return 1;
}
