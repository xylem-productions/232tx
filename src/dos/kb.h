#ifndef DOS_KB
#define DOS_KB

/* Check for keystroke */
static char KB_keystroke();
#pragma aux KB_keystroke = \
  "mov ah, 1" \
  "int 16h" \
  "mov al, 1" \
  "jnz nothing" \
  "mov al, 0" \
  "nothing:" \
  modify [ ax ] \
  value [ al ]

/* Fetch extended keystroke */
static int KB_getchar_ext();
#pragma aux KB_getchar_ext = \
  "mov ah, 10h" \
  "int 16h" \
  modify [ ax ] \
  value [ ax ]

#endif
