WATCOM_PATH ?= $(HOME)/opt/watcom-v2

BIN := bin
SRC := src

OBJS_CL := $(addsuffix .o,$(patsubst $(SRC)/common/%,$(BIN)/linux/%,$(basename \
 $(foreach s,$(SRC)/common,$(wildcard $s/*.c $s/*asm)))))
OBJS_LC := bin/linux/common.o bin/linux/rs232.o
OBJS_CD := $(addsuffix .o,$(patsubst $(SRC)/common/%,$(BIN)/dos/%,$(basename \
 $(foreach s,$(SRC)/common,$(wildcard $s/*.c $s/*asm)))))
OBJS_DC := bin/dos/common.o bin/dos/uarta.o bin/dos/uartc.o

WPATH := $(WATCOM_PATH)/binl:$(WATCOM_PATH)/binw
IPATH := $(WATCOM_PATH)/h:$(SRC)/common
LPATH := $(WATCOM_PATH)/lib286/dos
GCC := gcc
GCCFLAGS := -Wall -Wextra -Wpedantic -Werror -c
AS := nasm
ASFLAGS := -Werror -f obj
WCC := wcc
WCFLAGS := -0 -bt=dos -zdp -zfp -zq -ms -we -wx -zp=1
WLD := wlink
WLDFLAGS := sys dos option quiet option noredefsok option dosseg
BIN_LINUX := $(BIN)/232up $(BIN)/232dn
BIN_DOS := $(BIN)/232dn.exe $(BIN)/232up.exe $(BIN)/232dd.exe
BIN_ALL := $(BIN_LINUX) $(BIN_DOS)
PKG_DIR := 232tx
guard=@mkdir -p $(@D)

all: clean $(BIN_ALL) pkg

$(BIN)/linux/%.o: $(SRC)/linux/%.c
	$(guard)
	$(GCC) $(GCCFLAGS) -I $(SRC)/common -o $@ $<

$(BIN)/linux/%.o: $(SRC)/common/%.c
	$(guard)
	$(GCC) $(GCCFLAGS) -o $@ $<

$(BIN)/dos/%.o: $(SRC)/dos/%.asm
	$(guard)
	$(AS) $(ASFLAGS) -o $@ $<

$(BIN)/dos/%.o: $(SRC)/dos/%.c
	$(guard)
	PATH=$(WPATH) INCLUDE=$(IPATH) $(WCC) $(WCFLAGS) -fo=$@ $<

$(BIN)/dos/%.o: $(SRC)/common/%.c
	$(guard)
	PATH=$(WPATH) INCLUDE=$(IPATH) $(WCC) $(WCFLAGS) -fo=$@ $<

$(BIN)/232up: $(OBJS_LC) $(OBJS_CL) bin/linux/232up.o
	$(guard)
	gcc -o $@ $^

$(BIN)/232dn: $(OBJS_LC) $(OBJS_CL) bin/linux/232dn.o
	$(guard)
	gcc -o $@ $^

$(BIN)/232dn.exe: $(OBJS_DC) $(OBJS_CD) bin/dos/232dn.o
	$(guard)
	PATH=$(WPATH) INCLUDE=$(IPATH) WATCOM=$(WATCOM_PATH) \
	    $(WLD) libp $(LPATH) name $@ $(WLDFLAGS) \
	    file { $^ }

$(BIN)/232up.exe: $(OBJS_DC) $(OBJS_CD) bin/dos/232up.o
	$(guard)
	PATH=$(WPATH) INCLUDE=$(IPATH) WATCOM=$(WATCOM_PATH) \
	    $(WLD) libp $(LPATH) name $@ $(WLDFLAGS) \
	    file { $^ }

$(BIN)/232dd.exe: $(OBJS_DC) $(OBJS_CD) bin/dos/232dd.o
	$(guard)
	PATH=$(WPATH) INCLUDE=$(IPATH) WATCOM=$(WATCOM_PATH) \
	    $(WLD) libp $(LPATH) name $@ $(WLDFLAGS) \
	    file { $^ }

clean:
	rm -rf $(BIN)
	rm -rf $(PKG_DIR)

pkg: $(BIN_ALL)
	rm -rf $(PKG_DIR)
	mkdir $(PKG_DIR)
	cp -a $(BIN_ALL) README COPYING Makefile src $(PKG_DIR)
	tar -cvaf 232tx.tgz $(PKG_DIR)
	rm -rf $(PKG_DIR)

